// DOM- document object model

// Notes:
// querySelector function takes a string input that is formatted like CSS selector when applying styles. This allows to get a specific element
// we can contain the code inside a constant

// alternative
// document.getElementById("txt-first-name")
// document.getElementsByClassName()
// document.getElementsByTagName()

// console.log(document)

/*
	Mini-Activity
		*Target the element Full Name: and store it in a constant called spanFullName
*/
// Solution:

const txtFirstName = document.querySelector('#txt-first-name')
const spanFullName = document.querySelector('#span-full-name')



	txtFirstName.addEventListener('keyup', (event) => {
		spanFullName.innerHTML = txtFirstName.value;
})

	txtFirstName.addEventListener('keyup', (e) => {
		console.log(e.target)
		console.log(e.target.value)
})
