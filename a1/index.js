
const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName = document.querySelector('#txt-last-name')
const spanFullName = document.querySelector('#span-full-name')

function combine() {
	let firstName = txtFirstName.value
	let lastName = txtLastName.value
	spanFullName.innerHTML = `${firstName} ${lastName}`
}

txtFirstName.addEventListener('input', combine)
txtLastName.addEventListener('input', combine)